%% Wideband Signal
% I used the matlab help function and outside resources extensively for this
t=20000;
while(t<=80000)
fs = 1000;
x = randn(20000,1);

A=10+rand()*200;
B=A+rand()*200;

if((B-A)>=90)
    snr=1+20*rand();
    
    [y1,d1] = bandpass(x,[A B],fs,ImpulseResponse="iir",Steepness=[0.5 0.8]);
    ynew=awgn(y1,snr);
    
    win = hamming(128,"periodic");
    S=stft(y1,fs,Window=win,OverlapLength=40,FFTLength=128);
    Apple=abs(S);
    save(sprintf('WideBandSignal%d.mat',t),'Apple','-v4');
    t=t+1;
end

end