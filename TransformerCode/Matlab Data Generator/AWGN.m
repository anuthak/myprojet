%% AWGN Wave
% I used the matlab help function and outside resources extensively for this
Fs = 1000;
for t=20000:1:80000
    t1=0:0.01:3600*0.01;
    Nums=sin((0)*t1);
    snr=1+20*rand();

    ynew=awgn(Nums,snr);
    win = hamming(128,"periodic");
    S=stft(ynew,Fs,Window=win,OverlapLength=40,FFTLength=128);
    Apple=abs(S);
    save(sprintf('AWGN%d.mat',t),'Apple','-v4');
end
