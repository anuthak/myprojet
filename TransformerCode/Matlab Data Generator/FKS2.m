%% FM Modulator
% I used the matlab help function and outside resources extensively for this
for t=1:1:6670
    M = 2;       % Modulation order
    freqsep = 1+rand()*50; % Frequency separation (Hz)
    nsamp = 35;   % Number of samples per symbol
    Fs = 1000;     % Sample rate (Hz)
    x = randi([0 M-1],100,1);
    y = fskmod(x,M,freqsep,nsamp,Fs);
    
    snr=1+20*rand();

    Nums=zeros(3600,1);
    
    ynew=awgn(Nums,snr);
    
    ynew(1:length(y))=ynew(1:length(y))+y;

    win = hamming(128,"periodic");
    S=stft(ynew,Fs,Window=win,OverlapLength=40,FFTLength=128);
    Apple=abs(S);
    save(sprintf('2DataFSK%d.mat',t),'Apple','-v4');
end

%% FSK 4

for t=1:1:20000
    M = 4;       % Modulation order
    freqsep = 1+rand()*50; % Frequency separation (Hz)
    nsamp = 35;   % Number of samples per symbol
    Fs = 1000;     % Sample rate (Hz)
    x = randi([0 M-1],100,1);
    y = fskmod(x,M,freqsep,nsamp,Fs);
    
    snr=1+20*rand();

    Nums=zeros(3600,1);
    
    ynew=awgn(Nums,snr);
    
    ynew(1:length(y))=ynew(1:length(y))+y;

   
    win = hamming(128,"periodic");
    S=stft(ynew,Fs,Window=win,OverlapLength=40,FFTLength=128);
    Apple=abs(S);
    save(sprintf('4FSK%d.mat',t),'Apple','-v4');
end
%% FSK 8
for t=1:1:20000
    M = 8;       % Modulation order
    freqsep = 1+rand()*50; % Frequency separation (Hz)
    nsamp = 35;   % Number of samples per symbol
    Fs = 1000;     % Sample rate (Hz)
    x = randi([0 M-1],100,1);
    y = fskmod(x,M,freqsep,nsamp,Fs);
    
    snr=1+20*rand();

    Nums=zeros(3600,1);
    
    ynew=awgn(Nums,snr);
    
    ynew(1:length(y))=ynew(1:length(y))+y;

    win = hamming(128,"periodic");
    S=stft(ynew,Fs,Window=win,OverlapLength=40,FFTLength=128);
    Apple=abs(S);
    save(sprintf('8FSK%d.mat',t),'Apple','-v4');
end

%% FSK 16
for t=1:1:20000
    M = 16;       % Modulation order
    freqsep = 1+rand()*50; % Frequency separation (Hz)
    nsamp = 35;   % Number of samples per symbol
    Fs = 1000;     % Sample rate (Hz)
    x = randi([0 M-1],100,1);
    y = fskmod(x,M,freqsep,nsamp,Fs);
    
    snr=1+20*rand();

    Nums=zeros(3600,1);
    
    ynew=awgn(Nums,snr);
    
    ynew(1:length(y))=ynew(1:length(y))+y;

    win = hamming(128,"periodic");
    S=stft(ynew,Fs,Window=win,OverlapLength=40,FFTLength=128);
    Apple=abs(S);
    save(sprintf('16FSK%d.mat',t),'Apple','-v4');
end

