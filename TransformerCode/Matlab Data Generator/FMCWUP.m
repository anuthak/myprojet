%% FMCW 1
% I used the matlab help function and outside resources extensively for this
Fs = 1000;
for t=1:1:2000
st = [1e-3 2e-3];
bw = [1e5 9e4];
NumSweeps=4;
waveform = phased.FMCWWaveform('SweepTime',st,...
    'SweepBandwidth',bw,'SweepDirection','Up',...
    'SweepInterval','Symmetric','SampleRate',2e5,...
    'NumSweeps',NumSweeps);

x = waveform();

snr=1+20*rand();

Nums=zeros(3600,1);

xnew=awgn(Nums,snr);

xnew(1:length(x))=xnew(1:length(x))+x;
win = hamming(128,"periodic");
S=stft(xnew,Fs,Window=win,OverlapLength=40,FFTLength=128);
Apple=abs(S);
save(sprintf('4NewSweepUPFMCW%d.mat',t),'Apple','-v4');
end

%% FMCW 2
for t=1:1:2000
st = [1e-3 2e-3];
bw = [1e5 9e4];
NumSweeps=8;
waveform = phased.FMCWWaveform('SweepTime',st,...
    'SweepBandwidth',bw,'SweepDirection','Up',...
    'SweepInterval','Symmetric','SampleRate',2e5,...
    'NumSweeps',NumSweeps);

x = waveform();

snr=1+20*rand();

Nums=zeros(3600,1);

xnew=awgn(Nums,snr);

xnew(1:length(x))=xnew(1:length(x))+x;
win = hamming(128,"periodic");
S=stft(xnew,Fs,Window=win,OverlapLength=40,FFTLength=128);
Apple=abs(S);
save(sprintf('8NewSweepUPFMCW%d.mat',t),'Apple','-v4');
end



%% FMCW 3
for t=1:1:2000
st = [1e-3 2e-3];
bw = [1e5 9e4];
NumSweeps=10;
waveform = phased.FMCWWaveform('SweepTime',st,...
    'SweepBandwidth',bw,'SweepDirection','Up',...
    'SweepInterval','Symmetric','SampleRate',2e5,...
    'NumSweeps',NumSweeps);

x = waveform();

snr=1+20*rand();

Nums=zeros(3600,1);

xnew=awgn(Nums,snr);

xnew(1:length(x))=xnew(1:length(x))+x;
win = hamming(128,"periodic");
S=stft(xnew,Fs,Window=win,OverlapLength=40,FFTLength=128);
Apple=abs(S);
save(sprintf('10NewSweepUPFMCW%d.mat',t),'Apple','-v4');
end

%% FMCW 4
for t=1:1:660
st = [1e-3 2e-3];
bw = [1e5 9e4];
NumSweeps=2;
waveform = phased.FMCWWaveform('SweepTime',st,...
    'SweepBandwidth',bw,'SweepDirection','Up',...
    'SweepInterval','Symmetric','SampleRate',2e5,...
    'NumSweeps',NumSweeps);

x = waveform();

snr=1+20*rand();

Nums=zeros(3600,1);

xnew=awgn(Nums,snr);

xnew(1:length(x))=xnew(1:length(x))+x;
win = hamming(128,"periodic");
S=stft(xnew,Fs,Window=win,OverlapLength=40,FFTLength=128);
Apple=abs(S);
save(sprintf('2NewSweepUPFMCW%d.mat',t),'Apple','-v4');
end