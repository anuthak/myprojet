%% 2 CW Wave
% I used the matlab help function and outside resources extensively for this
Fs = 1000;

for t=1:1:20000
    t1=0:0.01:3599*0.01;
    Nums=sin((2+rand()*100)*t1);
    snr=1+20*rand();

    ynew=awgn(Nums,snr);
    win = hamming(128,"periodic");
    S=stft(ynew,Fs,Window=win,OverlapLength=40,FFTLength=128);
    Apple=abs(S);
    save(sprintf('CW%d.mat',t),'Apple','-v4');
end

%% 4 CW

Fs = 1000;  

for t=1:1:20000
    t1=0:0.01:3599*0.01;

    A=(2+rand()*100);
    B=(2+rand()*100);

    if(A~=B)
        Nums=sin(A*t1)+sin(B*t1);
        snr=1+20*rand();
    end

    ynew=awgn(Nums,snr);
    win = hamming(128,"periodic");
    S=stft(ynew,Fs,Window=win,OverlapLength=40,FFTLength=128);
    Apple=abs(S);
    save(sprintf('CW%d.mat',t),'Apple','-v4');
end

%% 6 CW

Fs = 1000;   

for t=1:1:20000
    t1=0:0.01:3599*0.01;

    A=(2+rand()*100);
    B=(2+rand()*100);
    C=(2+rand()*100);

    if(A~=B)
        if(B~=C)
            if(A~=C)
                Nums=sin(A*t1)+sin(B*t1)+sin(C*t1);
                snr=1+20*rand();
            end
        end
    end

    ynew=awgn(Nums,snr);
    win = hamming(128,"periodic");
    S=stft(ynew,Fs,Window=win,OverlapLength=40,FFTLength=128);
    Apple=abs(S);
    save(sprintf('CW%d.mat',t),'Apple','-v4');
end

%% 8 CW

Fs = 1000;     

for t=1:1:20000
    t1=0:0.01:3599*0.01;

    A=(2+rand()*100);
    B=(2+rand()*100);
    C=(2+rand()*100);
    D=(2+rand()*100);

    if(A~=B)
        if(B~=C)
            if(A~=C)
                if(A~=D)
                    if(D~=B)
                        if(D~=C)
                            Nums=sin(A*t1)+sin(B*t1)+sin(C*t1)+sin(D*t1);
                            snr=1+20*rand();
                        end
                    end
                end
            end
        end
    end

    ynew=awgn(Nums,snr);
    win = hamming(128,"periodic");
    S=stft(ynew,Fs,Window=win,OverlapLength=40,FFTLength=128);
    Apple=abs(S);
    save(sprintf('CW%d.mat',t),'Apple','-v4');
end
